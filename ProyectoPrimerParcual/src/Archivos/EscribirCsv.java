/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import Sistema.Administrador;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cesar
 */
public class EscribirCsv {
     /**

     * Método estático que no devuelve nada y permite crear los archivos csv con los datos

     * @param data ArrayList con la informacion dia a dia de la simulacion 
     * @param ciudadano String con el  porcentaje total de ciudadanos
     * @param policia String con el  porcentaje total de policias

     */
    public static void guardarArchivos(ArrayList<String> data,String ciudadano, String policia ){    
        String str[] = new String[data.size()];
        for(int i = 0; i < data.size(); i++){
            str[i] = data.get(i);
        }
        LocalDateTime local =  LocalDateTime.now();
        String fecha = "corridas"+".csv";
        String archCSV = "src/CSVS/"+fecha;
        try (CSVWriter writer = new CSVWriter(new FileWriter(archCSV))) {
            writer.writeNext(str);
        } catch (IOException ex) {
            Logger.getLogger(EscribirCsv.class.getName()).log(Level.SEVERE, null, ex);
        }
        String par[] = new String[6];
        par[0]= "Densidad %Policia: "+ policia;
        par[1]= "Densidad %Ciudadano: " + ciudadano;
        par[2]= "Dimension universo: " + String.valueOf(Administrador.DIMENSION) +  " "  + String.valueOf(Administrador.DIMENSION); 
        par[3]= "Vision de agentes: " + String.valueOf(Administrador.VISION);
        if(Administrador.RANGO_GOB){
            par[4]= "Legiblidad de gobierno por Rango: "+Administrador.GOB_INICIAL + "-" + Administrador.GOB_FINAL;        
        }
        else{
            par[4]= "Legiblidad de gobierno Fijo: "+Administrador.GOB_FIJO;        
        }
        par[5] = "Movimiento: "+ String.valueOf(Administrador.MOVIMIENTO);
        fecha = "parametros"+".csv";
        archCSV = "src/CSVS/"+fecha;
        try (CSVWriter wr = new CSVWriter(new FileWriter(archCSV))) {
            wr.writeNext(par);
        } catch (IOException ex) {
            Logger.getLogger(EscribirCsv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
