/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Sistema.Administrador;
import java.util.ArrayList;

/**
 *
 * @author Nicolas
 */
public  abstract class Agente {
    
    //campos de clase
    ArrayList<Agente> vecindario = new ArrayList<>();
    int vision;
    private int fila;
    private int columna;
    
    /**
    * Método que permite fijar la ubicacion del agente

     * @param fila número de fila en la que se encuentra
     * @param columna número de columna en la que se encuentra

     */
    public void ubicacion(int fila, int columna){
        this.fila = fila;
        this.columna = columna;
    }

    public int getVision() {
        return vision;
    }

    public void setVision(int vision) {
        this.vision = vision;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
    /**
    * Método que permite fijar el vecindario del agente con respecto a la visión del agente
     */
    public void calcularVecindario(){
        vecindario.clear();
        for(int rh = fila -vision; rh < fila+vision+1; rh ++ ){
            for(int rv = columna -vision; rv < columna+ vision+1; rv ++ ){
               if(rv>-1 && rh>-1 && rv<Administrador.DIMENSION && rh<Administrador.DIMENSION){
                   if(!(rh == fila && rv == columna)){
                       if(Administrador.campo.getCampo()[rh][rv].getAgente() != null){     
                            vecindario.add(Administrador.campo.getCampo()[rh][rv].getAgente());
                       } 
                   }      
               } 
           }
        }
    }

    public ArrayList<Agente> getVecindario() {
        calcularVecindario();
        return vecindario;
    }

    public void setVecindario(ArrayList<Agente> vecindario) {
        this.vecindario = vecindario;
    }
    
    
     
    
}
