/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Sistema.Administrador;

/**
 *
 * @author Cesar
 */
public class Policia  extends Agente{
    
    /**
     * Constructor para la clase Policia
     * @param vision radio que tendra su vecindario
     */
    public Policia(int vision){
        this.vision = vision;
        
    }
    
    public void Encarcelar(Ciudadano c){
        Administrador.llevarAPrision(c.getFila(), c.getColumna());
        c.encarcelado();
    }
}
