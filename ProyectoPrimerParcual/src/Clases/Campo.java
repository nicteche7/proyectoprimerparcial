/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import static Sistema.Administrador.DIMENSION;

/**
 *
 * @author pc
 */
public class Campo {
    
    //campos de la clase
    private int fila;
    private int columna;
    private Contenedor camp[][];
    
    /**

     * Constructor para la clase campo
     * @param fila numero de filas que tendra el campo
     * @param columna numero de columna que tendra el campo
     */
    public Campo (int fila, int columna){
        this.fila = fila;
        this.columna = columna;
        camp= new Contenedor [fila][columna];
        llenarCampo();
    }

    public Contenedor[][] getCampo() {
        return camp;
    }
    
    /**
     * Método que llena la matriz de contenedor de objetos contenedores
     */
    public void llenarCampo(){
        for(int f =0 ; f<=fila-1; f++){
            for(int c=0; c<=columna-1; c++){
                camp[f][c]= new Contenedor();
            }
        }
    }
    
    /**
     * Método que recorre todos los contenedores de la matriz y pone como null  el campo agente de estos
     */
    public void limpiarCampo(){
        for(int f =0 ; f<=fila-1; f++){
            for(int c=0; c<=columna-1; c++){
                camp[f][c].setAgente(null);
            }
        }
    }
    
    
    
    
}
