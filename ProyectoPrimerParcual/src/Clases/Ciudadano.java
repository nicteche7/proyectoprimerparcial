/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Sistema.Administrador;

/**
 *
 * @author Cesar
 */
public class Ciudadano extends Agente{
    
    //campos de clase
    private float agravio;
    private float probDetencion;  
    private float riesgo_neto;
    private final float aversion_riesgo;
    private final float perjuicioPercibido;
    private boolean rebelde;
    private boolean encarcelado;
    private int diasDetencion;
    private float C_A;
    
    /**
     * Constructor para la clase ciudadano
     * @param vision radio que tendra su vecindario
     */
    public Ciudadano(int vision){
        perjuicioPercibido = (float) Math.random(); //realizar calculo respectivo
        aversion_riesgo = (float) Math.random();
        this.vision= vision;
    }

    public boolean isRebelde() {
        return rebelde;
    }
    
    /**
     * Método que establace si el ciudadano es o no rebelde actualmente
     */
    public void rebelion(){
        riesgoNeto();
        agravio();
        if (agravio - riesgo_neto > Administrador.LIMITE){
            rebeldiaON();
        }
        else{
            rebeldiaOFF();
        }
    }
    
    /**
     * Método que establace el agravio del ciudadano
     */
    public void agravio(){
        agravio = (float) (perjuicioPercibido *(1-Administrador.legitimidadGobierno()));
    }
    
    /**
     * Método que establace el riesgoNeto del ciudadano
     */
    private void riesgoNeto(){
        calcularProbdetencion();
        riesgo_neto = aversion_riesgo * probDetencion;
    }
    
    /**
     * Método que establece la probabilidad de detención
     */
    private void calcularProbdetencion(){
        calculoProporcionPoliciaCiudadano();
        probDetencion = (float) (1 - Math.exp(-Administrador.K*Math.round(C_A)));
    }
    
    public void rebeldiaON(){
        rebelde = true;
    }
    
    public void rebeldiaOFF(){
        rebelde = false;
    }
    
    public boolean isEncarcelado() {
        return encarcelado;
    }

    public void encarcelado() {
        this.encarcelado = true;
        this.diasDetencion =  (int)(Math.random()*Administrador.MAX_TIEMPO_CARCEL +1);
    }
    
    public void libre() {
        this.encarcelado = false;
    }
    
    /**
     * Método que establece la proporción de policias con respecto a los ciudadanos
     */
    public void calculoProporcionPoliciaCiudadano(){
        int policias = 0;
        int rebeldes = 0;
        for(Agente a: vecindario){
            if(a instanceof Ciudadano){
                Ciudadano c = (Ciudadano)a;
                if(c.isRebelde()){
                    rebeldes++;
                }
            }
            else{
                policias++;
            }
        }
        if(rebeldes> 0){
            C_A= policias/rebeldes;
        }
        C_A = 0;
        
    }
    
    /**
     * Método que reduce sentencia
     */
    public void disminuirSentencia(){
        this.diasDetencion--;
    }

    public int getDiasDetencion() {
        return diasDetencion;
    }

    public void setDiasDetencion(int diasDetencion) {
        this.diasDetencion = diasDetencion;
    }
    
    

    
    
}
