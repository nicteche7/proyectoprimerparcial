/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inicial;

import Archivos.EscribirCsv;
import Sistema.Administrador;
import java.util.Scanner;
import Analisis.*;

/**
 *
 * @author Nico
 */
public class Simulacion {
    
    /**
     * Método main donde se pide al usuarios los parametros y llama al método que inicia la simulación
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("BIENVENIDO A LA SIMULACION");
        String policial="";
        while(!Administrador.verificarNumero(policial)){
            System.out.print("Ingrese porcentaje de densidad policial: ");
            policial = teclado.nextLine();
        }
        String ciudadano="";
        while(!Administrador.verificarNumero(ciudadano)){
            System.out.print("Ingrese porcentaje de densidad ciudadana: ");
            ciudadano = teclado.nextLine();
        }
        String dimension="";
        while(!Administrador.verificarNumero(dimension)){
            System.out.print("Ingrese dimension del universo( si sobrepasa 50) se asignara 50 como valor fijo: ");
            dimension = teclado.nextLine();
        }
        String vision="";
        while(!Administrador.verificarNumero(vision)){
            System.out.print("Ingrese visión de agentes: ");
            vision = teclado.nextLine();
        }
        String movimiento="";
        while(!"si".equals(movimiento) && !"no".equals(movimiento)){
            System.out.print("Desea movilidad en los agentes de la simulación( si | no ): ");
            movimiento = teclado.nextLine().toLowerCase();
        }
        String turnos="";
        while(!Administrador.verificarNumero(turnos)){
            System.out.print("Ingrese cantidad maxima de turnos: ");
            turnos = teclado.nextLine();
        } 
        String menu="";
        while(!"si".equals(menu) && !"no".equals(menu)){
            System.out.print("Desea una legitimidad de gobierno con rango(si - no): ");
            menu = teclado.nextLine().toLowerCase();
        }
        if(menu.equals("si")){
            Administrador.RANGO_GOB = true;
            String gobInicio="";
            while(!Administrador.verificarFloat(gobInicio)){
                System.out.print("Ingrese porcentaje entre 0 - 0.99 de legitimidad de gobierno para rango inicial: ");
                gobInicio = teclado.nextLine();
            } 
            String gobFinal="";
            while(!Administrador.verificarFloat(gobFinal)){
                System.out.print("Ingrese porcentaje entre 0 - 0.99 de legitimidad de gobierno para rango final: ");
                gobFinal = teclado.nextLine();
            } 
            Administrador.GOB_INICIAL = Float.parseFloat(gobInicio);
            Administrador.GOB_FINAL = Float.parseFloat(gobFinal);
            
        }
        else{
            Administrador.RANGO_GOB = false;
            String gobFijo="";
            while(!Administrador.verificarFloat(gobFijo)){
                System.out.print("Ingrese porcentaje entre 0 - 0.99 de legitimidad de gobierno: ");
                gobFijo = teclado.nextLine();
            } 
            Administrador.GOB_FIJO = Float.parseFloat(gobFijo);
        }
        iniciarSimulacion(ciudadano,policial,dimension,vision,movimiento,turnos);
        
        AnalisisRebeldeVSLibertad.main();
    }
    
    /**
     * Método que inicia simulacion 
     * @param ciudadano String probabilidad de ciudadanos
     * @param policia String probabilidad de policia
     * @param dimension  String con la dimensión de la simulación
     * @param vision String con el rango para los agentes de la simulación
     * @param movimiento String que dictamina si se mueven o no los agentes en la simulación
     * @param turnos String que indica cuantos dias o turnos funciona la simulación
     */
    public static void iniciarSimulacion(String ciudadano, String policia, String dimension, String vision, String movimiento, String turnos){
        Administrador.TURNOS = Integer.parseInt(turnos);
        Administrador.MOVIMIENTO = movimiento.equals("si");
        Administrador.VISION = Integer.parseInt(vision);
        Administrador.DIMENSION =  Integer.parseInt(dimension);
        Administrador.generarMatriz();
        Administrador.asignacionPorcentajeAgente(Float.parseFloat(policia),Float.parseFloat(ciudadano));
        Administrador.ordenarMundo();
        System.out.println("Informacion");
        System.out.println("P significa Policia");
        System.out.println("C significa Ciudadano");
        System.out.println("R significa Rebelde");
        System.out.println("0 significa casilla disponible");
        System.out.println("Estado Inicial de la simulación" );
        System.out.println("Estado Inicial de la simulación" );
        Administrador.imprimirCampo();
        for(int i = 0; i<Administrador.TURNOS; i++){ 
            System.out.printf("TURNO #%d \n",i+1);
            Administrador.redadaPolicial();
            Administrador.reducirDiasCarcel();
            Administrador.rebelarse();
            Administrador.imprimirCampo();
            if(Administrador.MOVIMIENTO){
                Administrador.ordenarMundo();
            }
            Administrador.sobreescribirData(i+1);
            
        }
        EscribirCsv.guardarArchivos(Administrador.DATA,ciudadano,policia);
        
               
    }
    
    
    
    
}
