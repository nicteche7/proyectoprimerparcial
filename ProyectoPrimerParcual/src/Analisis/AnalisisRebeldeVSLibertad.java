/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisis;

import Sistema.Administrador;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Nico
 */
public class AnalisisRebeldeVSLibertad  extends Application{
    
    public ObservableList<XYChart.Series<String, Number>> getData(){
        ArrayList<String> data = Administrador.DATA;
        
        XYChart.Series<String, Number> normales = new XYChart.Series<>();
        normales.setName("Rebeldes libres");
        

        XYChart.Series<String, Number> rebelados = new XYChart.Series<>();
        rebelados.setName("Rebeldes presos");
        
        for(int i = 0; i < data.size(); i++){
             String [] d = data.get(i).split(Pattern.quote("-"));
             normales.getData().add(new XYChart.Data<>(d[0],Integer.parseInt(d[2])));
             rebelados.getData().add(new XYChart.Data<>(d[0],Integer.parseInt(d[3])));
         }
        ObservableList<XYChart.Series<String, Number>> datax = FXCollections.observableArrayList();
        datax.addAll(normales,rebelados);
        System.out.println(datax);
        return datax;
        
        
    }

    @Override
    public void start(Stage primaryStage) {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Turnos");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Personas");
        BarChart chart = new BarChart(xAxis, yAxis);
        chart.setTitle("Gráfica de simulación de rebeldia");
        chart.setData(getData());
        StackPane root = new StackPane();
        root.getChildren().add(chart);
        Scene scene = new Scene(root, 640, 427);
        primaryStage.setTitle("JavaFX BarChart");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    public static void main() {
           launch();
     }
}
