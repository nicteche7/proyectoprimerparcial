/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Clases.Agente;
import Clases.Campo;
import Clases.Ciudadano;
import Clases.Contenedor;
import Clases.Policia;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author Nico
 */
public class Administrador {
   
   //campos de variables estaticas
    public static boolean RANGO_GOB;
    
    public static int POBLACION = 100;
    
    public static double LIMITE =  0.1;
    
    public static int MAX_TIEMPO_CARCEL =3;
    
    public static float GOB_FIJO;
    
    public static float GOB_INICIAL;
    
    public static float GOB_FINAL;
    
    public static ArrayList<Agente> AGENTES = new ArrayList<>();
    
    public static int DIMENSION;
    
    public static int VISION;
    
    public static int TURNOS;
    
    public static boolean MOVIMIENTO;
    
    public static Campo campo;
    
    public static float K = (float) 2.3;
    
    public static ArrayList<String> DATA = new ArrayList<>();
    
    
    /**
     * Método que verifica si un string es un número
     * @param s String que se quiere verificar
     * @return boolean que indica si era o un número
     */
    public static boolean verificarNumero(String s){
        boolean control; 
        try{
            Integer.parseInt(s);
            control = true;
         }
         catch(NumberFormatException e){
             control = false;
         }
        return control;
    }
    
    /**
     * Método que verifica si un string es un float
     * @param s String que se quiere verificar
     * @return boolean que indica si era o un float
     */
    public static boolean verificarFloat(String s){
        boolean control; 
        try{
            Float.parseFloat(s);
            control = true;
         }
         catch(NumberFormatException e){
             control = false;
         }
        return control;
    }
    
    /**
     * Método que asigna cuantos agentes seran policias y ciudadanos
     * @param policia indica porcentje a obtener de policias
     * @param ciudadano indica porcentje a obtener de ciudadanos
     */
    public static void asignacionPorcentajeAgente(float policia,float ciudadano){
        if(policia + ciudadano > 100){
            ciudadano = 100- policia; 
        }
        int c = (int) ( POBLACION * (ciudadano/100)); //numero de ciudadano
        int p = (int) (POBLACION  * (policia/100)); //numero de policias
        for(int i = 0; i< c; i++){
            AGENTES.add(new Ciudadano(VISION));
        }
        for(int i = 0; i< p; i++){
            AGENTES.add(new Policia (VISION));
        }
    }
    
    /**
     * Método  estático que asigna en su respectiva casilla a los agentes
     */
    public static void asignarEspacio(){
        ArrayList<Integer> espacios = new ArrayList<>();
        for (Agente a: AGENTES){
            int numero = (int)(Math.random()*(DIMENSION*DIMENSION));
            while(espacios.contains(numero)){
                numero = (int)(Math.random()*(DIMENSION*DIMENSION));
            }
            espacios.add(numero);
            ArrayList<Integer> fc = filasColumnas(numero);
            a.ubicacion(fc.get(0), fc.get(1));
        }
        
    }
    /**
     * Método  estático que genera la matriz campo
     */
    public static void generarMatriz(){
        if (DIMENSION>50){
            DIMENSION=50;
        }
        campo = new Campo(DIMENSION,DIMENSION);
    }
    
    /**
     * Método  estático que asigna los agentes en su respectivo contenedor teniendo encuenta su casilla asignada
     */
    public static void ordenarMundo(){
        asignarEspacio();
        campo.limpiarCampo();
        Collections.shuffle(AGENTES);
        for(Agente a: AGENTES){
            if(a instanceof Ciudadano){
                Ciudadano c = (Ciudadano)a;
                if(!c.isEncarcelado()){
                    campo.getCampo()[a.getFila()][a.getColumna()].setAgente(a);
                }
            }
            else{
                campo.getCampo()[a.getFila()][a.getColumna()].setAgente(a);
            }
        }    
    }
    
    /**
     * Método  estático que reduce a todos los presos un día de su pena
     */
    public static void reducirDiasCarcel(){
         for(Agente a: AGENTES){
            if(a instanceof Ciudadano){
                Ciudadano c = (Ciudadano)a;
                if(c.isEncarcelado()){
                    c.disminuirSentencia();
                    if(c.getDiasDetencion()== 0){
                        c.libre();  
                        reincoporarCiudadano(c);
                    }
                }
            }
        }
    }
    
    /**
     * Método  estático que reincorpora a los presos a la sociedad si ya han cumplido su condena
     */
    public static boolean reincoporarCiudadano(Ciudadano ciudadano){
        for(int f =0 ; f<=DIMENSION-1; f++){
            for(int c=0; c<=DIMENSION-1; c++){
                if(campo.getCampo()[f][c].getAgente()== null){
                    ciudadano.setFila(f);
                    ciudadano.setColumna(c);
                    campo.getCampo()[f][c].setAgente( (Agente) ciudadano);
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Método  estático que transforma un número n a su posicion de manera matricial
     * @param n número en la matriz del cual queremos obtener su posición
     * @return ArrayList<Integer> con la posicion : fila = 0 , columna =1;
     */
    public static ArrayList<Integer> filasColumnas(int n){
        ArrayList<Integer> i = new ArrayList<>();
        i.add((int)n/DIMENSION);
        i.add(n % DIMENSION);
        return i;
    }
    /**
     * Método  estático que calcula si un ciudadano cambia su postura con respecto a la rebelarse.
     */
    public static void rebelarse(){
        for(int f =0 ; f<=DIMENSION-1; f++){
            for(int c=0; c<=DIMENSION-1; c++){
                if(campo.getCampo()[f][c]!= null){
                    Agente a = campo.getCampo()[f][c].getAgente();
                    if(a instanceof Ciudadano){
                        Ciudadano ciudadano = (Ciudadano) a;
                        ciudadano.rebelion();
                    }
                }
            }
        }    
    }
    
    /**
     * Método  estático que limpia la casilla donde se llevara a alguien detenido
     */
    public static void llevarAPrision(int fila,int columna){
        campo.getCampo()[fila][columna].setAgente(null);
    }
    
    /**
     * Método estático que recorre por todos los policias y les permite coger preso a ciudadanos rebelados
     */
    public static void redadaPolicial(){ //policias buscan ciudadanos en su campo de vision
        for(Agente a: AGENTES){
            if(a instanceof Policia){
                Policia poli = (Policia) a;
                ArrayList<Ciudadano> rebelados = new ArrayList<>();
                for(Agente ag: poli.getVecindario()){
                    if(ag instanceof Ciudadano){
                        Ciudadano ciudadano = (Ciudadano) ag;
                        if(ciudadano.isRebelde()){
                            rebelados.add(ciudadano);
                        }
                    }
                }
                if(rebelados.size()>0){
                    Collections.shuffle(rebelados);
                    Random rnd = new Random();
                    Ciudadano c = rebelados.get(rnd.nextInt(rebelados.size()));
                    poli.Encarcelar(c);
                }
                
            }
        }
    }
    
    /**
     * Método  estático que imprime la forma actual de la matriz
     */
    public static void imprimirCampo(){
        for(int f =0 ; f<=DIMENSION-1; f++){
            for(int c=0; c<=DIMENSION-1; c++){
                if(campo.getCampo()[f][c].getAgente()!= null){
                    Agente a = campo.getCampo()[f][c].getAgente();
                    if(a instanceof Ciudadano){
                        Ciudadano ciudadano = (Ciudadano) a;
                        if(ciudadano.isRebelde()){
                            System.out.print("\u001B[31mR ");
                        }else{
                            System.out.print("\u001B[33mC ");
                        }
                    }
                    else{
                        System.out.print("\u001B[34mP ");
                    }
                }
                else{
                    System.out.print("\u001B[32m0 ");
                }               
            }
            System.out.println("");
        }
    }
    
    /**
     * Método  estático que devuelve la legitimidad del gobierno en base a si es un rango o fija
     * @return float que indica legitimidad del gobierno calculada o fijada.
     */
    public static float  legitimidadGobierno(){
        float valor=0;
        if(RANGO_GOB == true){
            valor = (float) (Math.random()*GOB_FINAL)+GOB_INICIAL;
        }else{
            valor = GOB_FIJO;
        }
        return valor;
    }
    
    /**
     * Método  estático que adiciona al ArrayList DATO la informción global al momento de finalizar el turno
     */
    public static void sobreescribirData(int turno){
        int agentesTranquilos = 0;
        int agentesRebelandose = 0;
        int agentesCarcel = 0;
        for(Agente a: AGENTES){
            if(a instanceof Ciudadano){
                Ciudadano ciudadano = (Ciudadano) a;
                if(ciudadano.isEncarcelado()){
                    agentesCarcel++;
                }
                else if(ciudadano.isRebelde()){
                    agentesRebelandose++;
                }
                else{
                    agentesTranquilos++;
                }
            }
        }
        String d = String.valueOf(turno)+"-"+String.valueOf(agentesTranquilos)+"-"+String.valueOf(agentesRebelandose)+"-"+String.valueOf(agentesCarcel)+"-";
        DATA.add(d);
    }
        

}
